"use strict";

function createNewUser() {
  let newUser = {
    firstName: prompt("What is your first name?"),
    lastName: prompt("What is your last name?"),
    birthday: prompt("What is your  birthday date in format dd.mm.yyyy?"),
    getLogin() {
      return (this.firstName[0] + this.lastName).toLowerCase();
    },
    getAge() {
      let datam = this.birthday.split(".").reverse().join("-");

      return parseInt(
        (Date.now() - new Date(datam)) / (1000 * 60 * 60 * 24 * 365.25)
      );
    },
    getPassword() {
      return (
        this.firstName[0].toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthday.split(".")[2]
      );
    },
  };

  return newUser;
}
let user = createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());
